<?php
/**
 * @file
 * book_feature.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function book_feature_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:book:default';
  $panelizer->title = 'Standard';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'book';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'book_feature_two';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
      'side' => NULL,
      'content' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '112abf75-3fbb-49ca-8349-379cdf701165';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-5c33d526-eb81-4067-8441-f31dd46ef1c9';
    $pane->panel = 'content';
    $pane->type = 'node_content';
    $pane->subtype = 'node_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'links' => 0,
      'no_extras' => 1,
      'override_title' => 0,
      'override_title_text' => '',
      'identifier' => '',
      'link' => 0,
      'leave_node_title' => 0,
      'build_mode' => 'full',
      'context' => 'panelizer',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5c33d526-eb81-4067-8441-f31dd46ef1c9';
    $display->content['new-5c33d526-eb81-4067-8441-f31dd46ef1c9'] = $pane;
    $display->panels['content'][0] = 'new-5c33d526-eb81-4067-8441-f31dd46ef1c9';
    $pane = new stdClass();
    $pane->pid = 'new-a07312c8-29ee-41cf-abbc-5d79816da022';
    $pane->panel = 'content';
    $pane->type = 'block';
    $pane->subtype = 'book_helper-book-helper-inline-navigation';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'a07312c8-29ee-41cf-abbc-5d79816da022';
    $display->content['new-a07312c8-29ee-41cf-abbc-5d79816da022'] = $pane;
    $display->panels['content'][1] = 'new-a07312c8-29ee-41cf-abbc-5d79816da022';
    $pane = new stdClass();
    $pane->pid = 'new-e3b0ed14-f252-42b2-943c-3966a34b5ebe';
    $pane->panel = 'content';
    $pane->type = 'node_links';
    $pane->subtype = 'node_links';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'build_mode' => 'full',
      'identifier' => '',
      'link' => 1,
      'context' => 'panelizer',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_class' => 'link-wrapper',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'e3b0ed14-f252-42b2-943c-3966a34b5ebe';
    $display->content['new-e3b0ed14-f252-42b2-943c-3966a34b5ebe'] = $pane;
    $display->panels['content'][2] = 'new-e3b0ed14-f252-42b2-943c-3966a34b5ebe';
    $pane = new stdClass();
    $pane->pid = 'new-5bc963fc-b4ff-4fa7-806d-3d4eb223529a';
    $pane->panel = 'content';
    $pane->type = 'node_comment_wrapper';
    $pane->subtype = 'node_comment_wrapper';
    $pane->shown = TRUE;
    $pane->access = array(
      'logic' => 'and',
    );
    $pane->configuration = array(
      'mode' => '1',
      'comments_per_page' => '10',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '5bc963fc-b4ff-4fa7-806d-3d4eb223529a';
    $display->content['new-5bc963fc-b4ff-4fa7-806d-3d4eb223529a'] = $pane;
    $display->panels['content'][3] = 'new-5bc963fc-b4ff-4fa7-806d-3d4eb223529a';
    $pane = new stdClass();
    $pane->pid = 'new-82a6e796-399a-423a-a47d-e82dc63966c1';
    $pane->panel = 'side';
    $pane->type = 'block';
    $pane->subtype = 'book_feature-book_feature_current';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '82a6e796-399a-423a-a47d-e82dc63966c1';
    $display->content['new-82a6e796-399a-423a-a47d-e82dc63966c1'] = $pane;
    $display->panels['side'][0] = 'new-82a6e796-399a-423a-a47d-e82dc63966c1';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:book:default'] = $panelizer;

  return $export;
}
