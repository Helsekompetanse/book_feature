<?php
/**
 * @file
 * book_feature.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function book_feature_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'book_helper_links';
  $strongarm->value = array(
    0 => 'book_add_child',
  );
  $export['book_helper_links'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'book_helper_navigation_options';
  $strongarm->value = array(
    0 => 'prev',
    1 => 'next',
    2 => 'up',
  );
  $export['book_helper_navigation_options'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_book';
  $strongarm->value = '1';
  $export['comment_book'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mmenu_item_mmenu_bottom';
  $strongarm->value = array(
    'enabled' => '1',
    'title' => 'Bottom menu',
    'name' => 'mmenu_bottom',
    'blocks' => array(
      0 => array(
        'module_delta' => 'book_feature|book_feature_current',
        'menu_parameters' => array(
          'min_depth' => '2',
        ),
        'title' => '',
        'collapsed' => '0',
        'wrap' => '1',
        'module' => 'book_feature',
        'delta' => 'book_feature_current',
      ),
    ),
    'options' => array(
      'classes' => 'mm-basic',
      'effects' => array(),
      'slidingSubmenus' => FALSE,
      'clickOpen' => array(
        'open' => TRUE,
        'selector' => 'footer.mobile-footer',
      ),
      'counters' => array(
        'add' => FALSE,
        'update' => TRUE,
      ),
      'dragOpen' => array(
        'open' => TRUE,
        'pageNode' => 'body',
        'threshold' => 100,
        'maxStartPos' => 50,
      ),
      'footer' => array(
        'add' => FALSE,
        'content' => '',
        'title' => '',
        'update' => TRUE,
      ),
      'header' => array(
        'add' => TRUE,
        'content' => '',
        'title' => '',
        'update' => TRUE,
      ),
      'labels' => array(
        'collapse' => FALSE,
      ),
      'offCanvas' => array(
        'enabled' => TRUE,
        'modal' => FALSE,
        'moveBackground' => TRUE,
        'position' => 'bottom',
        'zposition' => 'front',
      ),
      'searchfield' => array(
        'add' => FALSE,
        'addTo' => 'menu',
        'search' => FALSE,
        'placeholder' => 'Søk',
        'noResults' => 'Ingen resultater funnet.',
        'showLinksOnly' => TRUE,
      ),
    ),
    'configurations' => array(
      'clone' => FALSE,
      'preventTabbing' => FALSE,
      'panelNodetype' => 'div, ul, ol',
      'transitionDuration' => 400,
      'classNames' => array(
        'label' => 'Label',
        'panel' => 'Panel',
        'selected' => 'Selected',
        'buttonbars' => array(
          'buttonbar' => 'anchors',
        ),
        'counters' => array(
          'counter' => 'Counter',
        ),
        'fixedElements' => array(
          'fixedTop' => 'FixedTop',
          'fixedBottom' => 'FixedBottom',
        ),
        'footer' => array(
          'panelFooter' => 'Footer',
        ),
        'header' => array(
          'panelHeader' => 'Header',
          'panelNext' => 'Next',
          'panelPrev' => 'Prev',
        ),
        'labels' => array(
          'collapsed' => 'Collapsed',
        ),
        'toggles' => array(
          'toggle' => 'Toggle',
          'check' => 'Check',
        ),
      ),
      'dragOpen' => array(
        'width' => array(
          'perc' => 0.80000000000000004,
          'min' => 140,
          'max' => 440,
        ),
        'height' => array(
          'perc' => 0.80000000000000004,
          'min' => 140,
          'max' => 880,
        ),
      ),
      'offCanvas' => array(
        'menuInjectMethod' => 'prepend',
        'menuWrapperSelector' => 'body',
        'pageNodetype' => 'div',
        'pageSelector' => 'body > div',
      ),
    ),
  );
  $export['mmenu_item_mmenu_bottom'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_book';
  $strongarm->value = 0;
  $export['node_submitted_book'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'page_manager_node_view_disabled';
  $strongarm->value = FALSE;
  $export['page_manager_node_view_disabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_defaults_node_book';
  $strongarm->value = array(
    'status' => 1,
    'view modes' => array(
      'page_manager' => array(
        'status' => 1,
        'default' => 1,
        'choice' => 0,
      ),
      'default' => array(
        'status' => 0,
        'default' => 0,
        'choice' => 0,
      ),
      'teaser' => array(
        'status' => 0,
        'default' => 0,
        'choice' => 0,
      ),
    ),
  );
  $export['panelizer_defaults_node_book'] = $strongarm;

  return $export;
}
