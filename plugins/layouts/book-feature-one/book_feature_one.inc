<?php

/**
 * implementation of hook_panels_layouts()
 */
// Plugin definition
$plugin = array(
  'title' => t('Single column'),
  'category' => t('Book'),
  'icon' => 'book_feature_one.png',
  'theme' => 'book_feature_one',
  'css' => 'css/book_feature_one.css',
  'regions' => array('content' => t('Content')),
);
