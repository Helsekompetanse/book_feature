<?php

// Plugin definition
$plugin = array(
  'title' => t('Two column'),
  'category' => t('Book'),
  'icon' => 'book_feature_two.png',
  'theme' => 'book_feature_two',
  'css' => 'css/book_feature_two.css',
  'regions' => array(
    'side' => t('Side'),
    'content' => t('Content')
  ),
);
