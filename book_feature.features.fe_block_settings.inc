<?php
/**
 * @file
 * book_feature.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function book_feature_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['book_feature-book_feature_books'] = array(
    'cache' => -1,
    'css' => NULL,
    'custom' => 0,
    'delta' => 'book_feature_books',
    'js' => NULL,
    'module' => 'book_feature',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'lillyhammer' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'lillyhammer',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
