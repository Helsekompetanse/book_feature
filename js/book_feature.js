Drupal.behaviors.book_feature = {
  attach: function (context, settings) {

    var $ = jQuery;

    var selectStr = 'div.block-book-feature li a.menu-link-icon';
    selectStr = selectStr + ', .pane-book-feature-book-feature-current li a.menu-link-icon';
    selectStr = selectStr + ', div[class^="pane-book-feature-book-feature-expandable-"] li a.menu-link-icon';
    selectStr = selectStr + ', div[class*=" pane-book-feature-book-feature-expandable-"] li a.menu-link-icon';
    selectStr = selectStr + ', section[class^="block-book-feature-book-feature-expandable-"] li a.menu-link-icon';
    selectStr = selectStr + ', section[class*=" block-book-feature-book-feature-expandable-"] li a.menu-link-icon';
    $(selectStr).click(function(evt){

      evt.stopPropagation();

      var li = $(this).closest('li');

      if (li.is('.collapsed, .expanded')) {
        li.toggleClass('expanded collapsed');
      }

      return false;
    });


    $(".mobile-footer").click(function(e){
      e.preventDefault();
      $("#mmenu_bottom").mmenu().trigger("open.mm");
    });


  }
};
